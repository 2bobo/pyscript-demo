# PyScriptの紹介
下書き

# お前誰よ
いつもの

# PyScriptとは？
https://pyscript.net/
PyCon US 2022でAnaconda社から発表のあった
PythonでWebアプリケーションが開発できる
Apache License 2.0ライセンス

https://github.com/pyscript/pyscript

## どうなってる
- ようわからんけどWASMとPyodideを組み合わせてる
https://anaconda.cloud/pyscript-python-in-the-browser
- なのでPythonの実際の動作部分はPyodideとなる
- PyScriptはPyodideを使いやすくしたイメージ

# 何ができる？
- ブラウザでPythonが動く
- Pythonのパッケージが使える
- Javascriptと連携できる
- 

# デモ
- チュートリアルを簡単にやってみた  
  https://github.com/pyscript/pyscript/blob/main/docs/tutorials/getting-started.md


## デモ1
- こんにちわ世界
- `<py-script>`タグの中にPythonコードを書く

## デモ2
- print文で表示できる

## デモ3
- 内容はデモ2と同じだけど、SysntaxErrorを発生させるとこんな感じになる

## デモ4
- 標準モジュールはimport文で呼び出し可能
- `pyscript.write`メソッドでidを指定してHTML内にPythonの出力を表示できる

## デモ5
- `pyscript.write`メソッドだでけなく、`<py-script output="hoge">`とった形でも指定可能
- この場合、最後に指定した変数がid:hogeになる
- print文で出力した内容も含まれるので出力全般が使われると思われる（よくわかってない）

## デモ6
- 3rdパーティーのモジュールはPyodideで対応されていれば`<py-env>`タグで指定して利用可能
- もしくは`.whl`ファイルに記載してもOK
- 利用可能モジュールの一覧  
  https://github.com/pyodide/pyodide/tree/main/packages

# その他
- `<py-config>`タグで設定なんかもできそう
- こんなタグでUIも作れるっぽい
	- `<py-inputbox>`
	- `<py-box>`
	- `<py-button>`
	- `<py-title>`
	
# まだ微妙なとこ
- 動作が遅い…
- 開発がAnaconda（個人的にはどうかな…と…）


おわり
